This is my attempt to solve the cryptochallenges. I do this in haskell, because I wanted
to deepen my knowledge of this languages. With that said I'm neither a haskell nor a crypto
expert. I suspect that's going to be an uncomfortable but interesting ride.

Development and testing
=======================

## Init the sandbox

    $ cabal sandbox init

## Running the Testsuite

    $ cabal install --only-dependencies --enable-tests
    $ ./run_tests
