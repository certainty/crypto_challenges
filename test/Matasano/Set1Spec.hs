{-# LANGUAGE OverloadedStrings #-}

module Matasano.Set1Spec (spec) where
import Test.Hspec
import Matasano.Set1

spec :: Spec
spec = challenge1 >> challenge2 >> challenge3

challenge1 :: Spec
challenge1 = do
  specify "challenge 1" $ do
    (solveChallenge1 base16) `shouldBe` base64
  where
    base16 = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
    base64 = "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"

challenge2 :: Spec
challenge2 = do
    specify "challenge 2" $ do
      (solveChallenge2 a b) `shouldBe` c
  where
    a = "1c0111001f010100061a024b53535009181c"
    b = "686974207468652062756c6c277320657965"
    c = "746865206b696420646f6e277420706c6179"

challenge3 :: Spec
challenge3 = do
  specify "challenge 3" $ do
    (solveChallenge3 ciphertext) `shouldBe` (Just plaintext)
  where
    ciphertext = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
    plaintext  = "Cooking MC's like a pound of bacon"
