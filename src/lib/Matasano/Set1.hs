{-# LANGUAGE OverloadedStrings #-}

module Matasano.Set1 where

import Utils.ByteString
import Utils.CryptoAnalysis.Xor
import Data.ByteString.Char8 (pack)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BS

bs :: String -> B.ByteString
bs = pack

-- | http://cryptopals.com/sets/1/challenges/1/
solveChallenge1 :: String -> String
solveChallenge1 = toBase64 . fromBase16

-- | http://cryptopals.com/sets/1/challenges/2/

solveChallenge2 :: String -> String -> String
solveChallenge2 a b = toBase16 $ a' `xor` b'
  where
    a' = fromBase16 a
    b' = fromBase16 b

-- | http://cryptopals.com/sets/1/challenges/3/

-- solveChallenge3 :: String -> Maybe String
-- solveChallenge3 cipher = fmap BS.unpack (breakXor1 (fromBase16 cipher))
