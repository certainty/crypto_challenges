module Utils.NaturalLanguage.Similarity where
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.List (sum, zipWith)

-- | Given two frequencies calculate the similarity between the them
--   Returns Nothing if the similarity could not be calculated because NaN was calculated
--   Returns Just v otherwise.
--   v is a real number between 0.0 and 1.0
--   0.0 means no similarity
---  1.0 means equality
frequencySimilarity :: (Ord k, RealFloat v) => (Map.Map k v) -> (Map.Map k v) -> Maybe v
frequencySimilarity m1 m2 = if (isNaN similarity) then Nothing else (Just similarity)
  where
    similarity = cosineSimilarity v1 v2
    (v1,v2)    = frequencyVectors m1 m2

-- | Given two frequencies calculate the vectors that represpents those frequencies.
--   Let K be the union of the sets of keys
--   Then the frequency vector V is the vector of values such that
--   V[i] is the frequency of K[i] in the inputs.
frequencyVectors :: (Ord k, Floating v) => (Map.Map k v) -> (Map.Map k v) -> ([v], [v])
frequencyVectors freqA freqB = (vectorOfA, vectorOfB)
  where
    vectorOfA       = map (lookup freqA) keys
    vectorOfB       = map (lookup freqB) keys
    keys            = Set.toList $ Set.union (Map.keysSet freqA) (Map.keysSet freqB)
    lookup dict key = Map.findWithDefault 0.0 key dict

-- | http://en.wikipedia.org/wiki/Cosine_similarity
cosineSimilarity :: (Floating a) => [a] -> [a] -> a
cosineSimilarity v1 v2 = (dotProduct v1 v2) / ((magnitude v1) * (magnitude v2))
  where
    dotProduct v1 v2 = sum $ Data.List.zipWith (*) v1 v2
    magnitude v      = sqrt $ dotProduct v v
