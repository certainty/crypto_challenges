module Utils.NaturalLanguage.English (properties) where
import Utils.NaturalLanguage (CorpusProperties(..), frequenciesFromList)

properties :: CorpusProperties
properties = CorpusProperties {
   size                   = Nothing
  , wordCount             = Nothing
  , whiteSpaceCount       = Nothing
  , containsNonPrintables = False
  , unigramInWordFreq     = Just $ frequenciesFromList englishUniInWord
  , unigramFreq           = Just $ frequenciesFromList englishUni
  , bigramInWordFreq      = Just $ frequenciesFromList englishBiInWord
  , bigramFreq            = Nothing
  }

-- Taken from: http://en.wikipedia.org/wiki/Letter_frequency [1]
englishUniInWord = [
  ("a", 8.167),("b", 1.492),("c", 2.782),("d", 4.253),("e", 12.702),
  ("f", 2.228),("g", 2.015),("h", 6.094),("i", 6.966),("j", 0.153),
  ("k", 0.772),("l", 4.025),("m", 2.406),("n", 6.749),("o", 7.507),
  ("p", 1.929),("q", 0.095),("r", 5.987),("s", 6.327),("t", 9.056),
  ("u", 2.758),("v", 0.978),("w", 2.361),("x", 0.150), ("y", 1.974),
  ("z", 0.074)
  ]

-- NOTE: the space is not mentioned explicitly there [1]. It's only stated that it is
-- slightly more frequent than E.
englishUni = (" ", 13.000) : englishUniInWord
-- Not used currently
-- (",", 4.700),
-- (".", 4.500),
-- ("-", 2.000),
-- ("(", 2.000),
-- (")", 2.000),
-- (";", 2.000),
-- ("?" , 1.000),
-- ("!", 1.000)

-- taken from http://norvig.com/mayzner.html [2]
englishBiInWord = [
  ("th", 3.56), ("he", 3.07), ("in", 2.43), ("er", 2.05),
  ("an", 1.99), ("re", 1.85), ("on", 1.76), ("at", 1.49),
  ("en", 1.45), ("nd", 1.35), ("ti", 1.34), ("es", 1.34),
  ("or", 1.28), ("te", 1.20), ("of", 1.17), ("ed", 1.17),
  ("is", 1.13), ("it", 1.12), ("al", 1.09), ("ar", 1.07),
  ("st", 1.05), ("to", 1.04), ("nt", 1.04), ("ng", 0.95),
  ("se", 0.93), ("ha", 0.93), ("as", 0.87), ("ou", 0.87),
  ("io", 0.83), ("le", 0.83), ("ve", 0.83), ("co", 0.79),
  ("me", 0.79), ("de", 0.76), ("hi", 0.76), ("ri", 0.73),
  ("ro", 0.73), ("ic", 0.70), ("ne", 0.69), ("ea", 0.69),
  ("ra", 0.69), ("ce", 0.65), ("li", 0.62), ("ch", 0.60),
  ("ll", 0.58), ("be", 0.58), ("ma", 0.57), ("si", 0.55),
  ("om", 0.55), ("ur", 0.54)
  ]
