module Utils.CryptoAnalysis.Xor where

import Utils.NaturalLanguage
import Utils.ByteString
import qualified Data.ByteString.Char8 as B
import Data.List (sortBy)
import Data.Char (isControl)
import Data.Word

-- | Attempt to break a singlebyte Xor.
--   It analyzes the input and if selects the best matching plaintext.
--   That is determined by running a statistical analysis on the outputs.
--   The plaintext with the highest similarity to english is returned.
-- breakXor1 :: B.ByteString -> Maybe B.ByteString
-- breakXor1 cipher = bestMatch $ analyzeXor1 cipher
--   where
--     bestMatch []                  = Nothing
--     bestMatch ((_,plaintext):_) = Just plaintext

-- | Analyze a cipher text that has bee produced
--   by applying a single byte xor to the plaintext.
--   It generates all possible plaintexts and runs a statistical
--   analysis on the output. It sorts the results by their similarity
--   to english text. The scores range between 0.0 and 1.0
-- analyzeXor1 :: B.ByteString -> [(Double, B.ByteString)]
-- analyzeXor1 cipher    = sortByScore scoredPlaintexts
--   where
--     sortByScore       = sortBy (applyFirst (flip compare))
--     scoredPlaintexts  = cleanup (map score plaintexts)
--     cleanup           = filter (not . isNaN . fst)
--     score input       = ((frequencySimilarity english (analyzeText input)), input)
--     plaintexts        = filter properText (map (flip xor1 cipher) keys)
--     keys              = [1..255] :: [Word8]
--     properText        = B.all (not . isControl)
