module Utils.ByteString where

import Prelude hiding (zipWith)
import Data.ByteString.Char8 (pack, unpack, zipWith)
import qualified Data.ByteString as B
import qualified Data.ByteString.Base64 as BS64
import qualified Data.ByteString.Base16 as BS16
import Control.Arrow ((&&&))
import Data.Word

import qualified Data.Bits as Bits
import Data.Char (ord, chr)

fromBase64 :: String -> B.ByteString
fromBase64 = BS64.decodeLenient . pack

toBase64 :: B.ByteString -> String
toBase64 = unpack . BS64.encode

fromBase16 :: String -> B.ByteString
fromBase16 = fst . BS16.decode . pack

toBase16 :: B.ByteString -> String
toBase16 = unpack . BS16.encode

base16ToBase64 :: String -> String
base16ToBase64 = toBase64 . fromBase16

-- | Xor two bytestrings with each other bytewisen
xor :: B.ByteString -> B.ByteString -> B.ByteString
xor a b = pack $ zipWith xor' a b
  where
    xor' a' b' = chr $ Bits.xor (ord a') (ord b')

-- | Xor the entire bytestring by xoring each byte with the given byte
xor1 :: Word8 -> B.ByteString -> B.ByteString
xor1 val = B.map (flip Bits.xor val)
