module Utils.NaturalLanguage where
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as BS
import Data.Char (chr, ord, isPrint, isSpace, toLower)
import Data.Word
import Utils.ByteString
import Data.List (zipWith, sum, map, filter, take, tails, sortBy)
import Control.Arrow (first, second, (&&&))
import qualified Data.Map as Map

-- TODO:
-- Find ways to make the language detection better.
-- Possible ideas are:
-- 1) Take whitespaces into account.
--    The character/whitespace ratio should be relatively even.
--    Most ciphertexts i've seen here doesn't have many whitespaces.
--    That might be an overlooked ratio
-- 2) Take n-grams into account
--    Have a frequency-table for N-grams.
--    I have statistics for Bi-and-Trigrams which an be used
--    in the same way as unigrams are used for similarity determination.
--    An optimization for bi/trigrams would be to only count them in words,
--    to avoid bi/tri-grams where a whitespace is the successor/predecessor
-- 3) A bonus based optimization (Thanks to scepticulous for the idea)
--    Rank the text as usual with unigrams and bi/tri-grams.
--    Then give bonus-points for texts that have common n-grams such
--    as they,them, their, and, or etc.

data CorpusProperties = CorpusProperties {
    size                  :: Maybe Int -- amount of bytes
  , wordCount             :: Maybe Int -- amount of words
  , whiteSpaceCount       :: Maybe Int -- amount of whitespaces
  , containsNonPrintables :: Bool -- does the corpus contain non-printables? (strong indicator for non human text)

  -- Note the ngrams here are byte-ngrams not word ngrams as used usually
  , unigramFreq           :: Maybe (Map.Map B.ByteString Double) -- relative unigram frequency
  , unigramInWordFreq     :: Maybe (Map.Map B.ByteString Double) -- relative unigram frequency in words (not counting whitespaces)
  , bigramFreq            :: Maybe (Map.Map B.ByteString Double) -- relative bigram frequency
  , bigramInWordFreq      :: Maybe (Map.Map B.ByteString Double) -- reltive bigram frequency in words (not counting whitespace)

  } deriving (Eq, Show)

fromChar :: Char -> Word8
fromChar = fromIntegral . ord

toChar :: Word8 -> Char
toChar = chr . fromIntegral

-- | generic ngrams function that calculates all ngrams in the input list
ngrams' :: Int -> [a] -> [[a]]
ngrams' n = filter ((==) n . length) . map (take n) . tails

-- | ngrams specialized for bytestrings
ngrams :: Int -> B.ByteString -> [B.ByteString]
ngrams n = map BS.pack . (ngrams' n) . BS.unpack

-- | count ngrams in all words in the input
-- | a word is simply any string of non-whitespace characters
wngrams :: Int -> B.ByteString -> [B.ByteString]
wngrams n bs = concat $ map (ngrams n) (BS.words bs)

-- | calculate the frequency of each element in the list
frequencies :: (Ord k) => [k] -> Map.Map k Int
frequencies = foldl count Map.empty
  where
    count m e = Map.insertWith (+) e 1 m

-- | Given a frequency make it relative to the total amount af tokens in the input
relativizeFrequencies :: (Integral i, Floating f) => (Int -> i -> f) -> Int -> (Map.Map k i) -> (Map.Map k f)
relativizeFrequencies rel total m = Map.map (rel total) m

relPercentage :: (Integral a, Floating r) => Int -> a -> r
relPercentage total count = (fromIntegral (100 * count)) / (fromIntegral total)

relCanonical :: (Integral a, Floating r) => Int -> a -> r
relCanonical total count = (fromIntegral count) / (fromIntegral total)

-- | given a bytestring returns an new bytestring with only printable bytes
textify :: B.ByteString -> B.ByteString
textify = B.filter (isPrint . toChar)

-- | remove whitespaces
compress :: B.ByteString -> B.ByteString
compress = B.filter (not . isSpace . toChar)

-- | downcase a bytestring
---  This is often used to normalize the input
downcase :: B.ByteString -> B.ByteString
downcase = B.map (fromChar . toLower . toChar)

-- | Analyze a corpus and assemble its properties
analyzeCorpus :: B.ByteString -> CorpusProperties
analyzeCorpus corpus = CorpusProperties {
  size                    = Just size'
  , wordCount             = Just wordcount'
  , whiteSpaceCount       = Just wscount
  , containsNonPrintables = containsNonPrint
  , unigramFreq           = Just unigramFreq'
  , bigramFreq            = Just bigramFreq'
  , unigramInWordFreq     = Just unigramInWordFreq'
  , bigramInWordFreq      = Just bigramInWordFreq'
  }
  where
    size'              = B.length corpus
    words              = BS.words corpus
    wsize'             = sum $ map B.length words
    wordcount'         = length words
    wscount            = B.length (B.filter (isSpace . toChar) corpus)
    containsNonPrint   = B.any (not . isPrint . toChar) corpus
    unigramFreq'       = relativeFreq size'  (ngrams  1 corpus)
    unigramInWordFreq' = relativeFreq wsize' (wngrams 1 corpus)
    bigramFreq'        = relativeFreq size'  (ngrams  2 corpus)
    bigramInWordFreq'  = relativeFreq wsize' (wngrams 2 corpus)
    relativeFreq total = (relativizeFrequencies relPercentage total) . frequencies

-- little helper to debug the corpus
showProps :: CorpusProperties -> String
showProps prop = unlines [
   "size:                  " ++ (show .size $ prop),
   "word count:            " ++ (show . wordCount $ prop),
   "whitespace count:      " ++ (show . whiteSpaceCount $ prop),
   "non-printables?:       " ++ (show . containsNonPrintables $ prop),
   "top5 unigrams:         " ++ (show $ fmap topN (unigramFreq prop)),
   "top5 unirams in words: " ++ (show $ fmap topN (unigramInWordFreq prop)),
   "top5 bigrams:          " ++ (show $ fmap topN (bigramFreq prop)),
   "top5 bigrams in words: " ++ (show $ fmap topN (bigramInWordFreq prop))
  ]
  where
    topN m = take 5 (sortBy (\(_, v) (_, v') -> compare v' v) (Map.toList m))

printProperties :: CorpusProperties -> IO ()
printProperties = putStrLn . showProps

frequenciesFromList :: [(String, Double)] -> Map.Map B.ByteString Double
frequenciesFromList input = Map.fromList $ map (first BS.pack) input

testString = BS.pack "This is my test. Mary had a little lamb ',.?!(){}~`&@[]+*_-;\""
